api = 2
; Drupal 7.14, but we roll out own.
core = 7.x

projects[libraries] = 1.0
projects[ip_anon] = 1.0
projects[botcha] = 1.0
projects[regcode] = 1.0
projects[password_policy] = 1.0
projects[flood_control] = 1.0
projects[encrypt_submissions] = 1.1
projects[search_restrict] = 1.0
projects[homographic_usernames] = 1.0
projects[paranoia] = 1.0-rc2
projects[logintoboggan] = 1.3
projects[systemmask] = 1.0-beta1
projects[securelogin] = 1.2
projects[ctools] = 1.0
projects[views] = 3.3
projects[addanother] = 2.1


projects[securepages][type] = module
projects[securepages][download][type] = get
projects[securepages][download][revision] = 398a20e3764317474c682ae7e811627f004ad2c1
projects[securepages][download][branch] = master

projects[protected_node][type] = module
projects[protected_node][download][type] = git
projects[protected_node][download][url] = http://drupalcode.org/project/securepages.git/snapshot/7b6cdf9f2cc709a5f8235b0d510dc7d3ee870299.tar.gz
